%% Initilazation
N=100;
npar=4; %number of parameters
%just for presentation reasons -> this is the changes
P0=1;
P=P0*eye(npar);
th=10*ones(1,npar);
Phi=zeros(1,npar);
Theta=zeros(N,npar);
LongP=zeros(N,npar);
Longpe=zeros(N,1);
lambda=0.5;
h=0.1;
%%

x = -pi:4*pi/N:3*pi-4*pi/N;
u = square(x)';
t = -h;
%%
VirtualMotor(u(1),0);
for k=2:N
   % fprintf(1,'\b\b\b\b\b\b  %.2d %%',floor(k/N*100)) %progress status
    t = t + h;                    % Continuous time update
   % ut = [t-h, u(k-1), noise(k)*1.0; t, u(k-1), noise(k)*1.0];
    [output,~,~]=VirtualMotor(u(k),k);
    y(k) = output;%%(length(output),1)';
    
    [th, P, pe]=rls_cd2017(th, Phi, P, y(k), npar,1);
    for i = 1:npar
        Theta(k, i)=th(i);
        LongP(k, i)=P(i,i);
    end
    Longpe(k)=pe;

    for j = 1:npar-1
        Phi(npar+1-j)=Phi(npar-j);
    end
    Phi(1)=-y(k);
    Phi(1+npar/2)=u(k);
end

Theta
%% to use in simulink 
num=[th(1,1), th(2,1)]
dnum= [1 ,th(3,1), th(4,1)]
gk=tf(num,dnum)
figure(6)
step(gk)


%% Plot
figure(1)
subplot(2,1,1), plot(Theta(:,1)), title('Theta 1')
subplot(2,1,2), plot(Theta(:,2)), title('Theta 2')
figure(2)
subplot(2,1,1), plot(Theta(:,3)), title('Theta 3')
subplot(2,1,2), plot(Theta(:,4)), title('Theta 4')
figure(3)
subplot(2,1,1), plot(LongP(:,1)), title('LongP')
subplot(2,1,2), plot(LongP(:,2)), title('LongP')
figure(4)
subplot(2,1,1), plot(Longpe(:,1)), title('LongPe')
figure(5)
plot(u);