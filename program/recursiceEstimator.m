function sysParamOut = recursiceEstimator(uk, yk)
%RECURSICEESTIMATOR Estimates the system parameter dependend on system
%input output signal and the supposed system order
    %% INIT:
    n = 2; % supposed system order
    adaptionfactor = 1;
    persistent Pkm1;
    persistent sysParam;
    persistent y;
    persistent u;
    if isempty(sysParam)
       sysParam = zeros(2*n, 1); 
       disp('Init var: sysParam.');
    end
    if isempty(Pkm1)
       Pkm1 = (10^5)*eye(2*n); 
       disp('Init var: Pkm1.');
    end
    if isempty(u)
       u = zeros(n+1, 1); 
       disp('Init var: u.');
    end
    if isempty(y)
       y = zeros(n+1, 1); 
       disp('Init var: y.');
    end
    
    %% UPDATE MEASUREMENT VEKTOR:
    y = circshift(y, 1);
    u = circshift(u, 1);
    y(1) = yk;
    u(1) = uk;
    
    %% UPDATE SYSTEM PARAMETER:
    mk = [-y(2:2+n-1)' u(2:2+n-1)']';
    gk = 1/(adaptionfactor+mk'*Pkm1*mk)*Pkm1*mk;
    Pkm1 = 1/adaptionfactor*(eye(2*n)-gk*mk')*Pkm1; % For next calculation    
    sysParam = sysParam + gk * ( yk - mk'*sysParam);
    
    %% OUT:
    sysParamOut = sysParam;
end

