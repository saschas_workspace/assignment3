N=1000;%samples
u=zeros(N,1);
y=zeros(N,1);
yest=zeros(N,1);
npar=4;
theta=zeros(1,npar)';
alpha=1e3;
p=alpha*eye(npar);
lambda=1;
i=0;
while i<N
    u(i+1)=sin(i);
    [output,ia,time]=VirtualMotor(u,i);
    y=output;
    if i<3
    i=i+1;
    else
        phit=[-y(i-1) -y(i-2) -u(i-1) -u(i-2)];
        phi=phit';
        %yest(i)=phit*p;
        p=(1/lambda)*(p-(p*phi*phit*p)/(lambda+phit*p*phi));
        theta=theta-p*phi*(phit*theta-y(i));
                i=i+1;
    end
    
end 

plot(yest,'r')
hold on
plot(y,'b')
grid