%% test data
h=0.1;
z=tf('z',0.1);
gz=(0.2)/(z + 0.125)
[output_g, t] = step(gz, 10);
output_g;

N=length(output_g);%samples
u=zeros(N,1);
y=zeros(N,1);
yest=zeros(N,1);
error=zeros(N,1);
npar=2;
theta=zeros(1,npar)';
alpha=100;
p=alpha*eye(npar)
q=eye(npar);
lambda=1;
i=3;

while i<N
    output=output_g(i);
    y(i)=output;
    phi=[-y(i-1) u(i-1)];
    q=p/(lambda+phi*p*phi');
    k=q*phi';
    p=(1/lambda)*(p-(p*(phi')*phi*p)/(1+phi*p*(phi')));
    theta=theta+k*(y(i)-phi*theta)
    yest(i)=phi*theta;
    error(i)=y(i)-yest(i);
    i=i+1;
    
end
%error=y-yest;
figure (1)
plot (error)
    figure (2)
    plot (y,'b')
     hold on
     plot (yest,'r')
% % %     figure (2)
% % %     plot (y,'b')
% % %     figure (3)
% % %     plot (yest,'r')