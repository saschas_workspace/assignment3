%% Initilazation
N=100;
npar=4; %number of parameters

P0=1;
P=P0*eye(npar);
th=zeros(1,npar);%0.5*ones(1,npar);
Phi=zeros(1,npar);
Theta=zeros(N,npar);
LongP=zeros(N,npar);
Longpe=zeros(N,1);
lambda=0.5;
h=0.1;
%%

x = -pi:4*pi/N:3*pi-4*pi/N;
u = square(x)';
t = -h;
noise=0;%.2+(-0.5-0.5).*rand(N,1); 
%%
for k=2:N
   % fprintf(1,'\b\b\b\b\b\b  %.2d %%',floor(k/N*100)) %progress status
    t = t + h;                    % Continuous time update
   % ut = [t-h, u(k-1), noise(k)*1.0; t, u(k-1), noise(k)*1.0];
    [output,~,~]=VirtualMotor(u(k),k-2);
    y(k) = output;%%(length(output),1)';
    
    [th1, th2, th3, th4]=recursiceEstimator(u(k),y(k))
    for i = 1:npar
        Theta(k, i)=th(i);
        LongP(k, i)=P(i,i);
    end
    Longpe(k)=pe;

    for j = 1:npar-1
        Phi(npar+1-j)=Phi(npar-j);
    end
    Phi(1)=-y(k);
    Phi(1+npar/2)=u(k);
end


%% to use in simulink 
num=[th1, th2]
dnum= [1 ,th3, th4]

%% Plot
figure(1)
subplot(2,1,1), plot(Theta(:,1)), title('Theta 1')
subplot(2,1,2), plot(Theta(:,2)), title('Theta 2')
figure(2)
subplot(2,1,1), plot(Theta(:,3)), title('Theta 3')
subplot(2,1,2), plot(Theta(:,4)), title('Theta 4')
figure(3)
subplot(2,1,1), plot(LongP(:,1)), title('LongP')
subplot(2,1,2), plot(LongP(:,2)), title('LongP')
figure(4)
subplot(2,1,1), plot(Longpe(:,1)), title('LongPe')
figure(5)
plot(u);

function sysParamOut = recursiceEstimator(uk, yk)
%RECURSICEESTIMATOR Estimates the system parameter dependend on system
%input output signal and the supposed system order
    %% INIT:
    n = 2; % supposed system order
    adaptionfactor = 1;
    persistent Pkm1;
    persistent sysParam;
    persistent y;
    persistent u;
    if isempty(sysParam)
       sysParam = zeros(2*n, 1); 
       disp('Init var: sysParam.');
    end
    if isempty(Pkm1)
       Pkm1 = (10^5)*eye(2*n); 
       disp('Init var: Pkm1.');
    end
    if isempty(u)
       u = zeros(n+1, 1); 
       disp('Init var: u.');
    end
    if isempty(y)
       y = zeros(n+1, 1); 
       disp('Init var: y.');
    end
    
    %% UPDATE MEASUREMENT VEKTOR:
    y = circshift(y, 1);
    u = circshift(u, 1);
    y(1) = yk;
    u(1) = uk;
    
    %% UPDATE SYSTEM PARAMETER:
    mk = [-y(2:2+n-1)' u(2:2+n-1)']';
    gk = 1/(adaptionfactor+mk'*Pkm1*mk)*Pkm1*mk;
    Pkm1 = 1/adaptionfactor*(eye(2*n)-gk*mk')*Pkm1; % For next calculation    
    sysParam = sysParam + gk * ( yk - mk'*sysParam);
    
    %% OUT:
    sysParamOut = sysParam;
end
