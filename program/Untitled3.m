N=1000;%samples
u=zeros(N,1);
y=zeros(N,1);
yest=zeros(N,1);
error=zeros(N,1);
npar=4;
theta=zeros(1,npar)';
alpha=100;
p=alpha*eye(npar);
q=eye(npar);
lambda=0.98;
i=3;
%%
%Controler
r1=0;
R=[1 r1];
s0=0;
s1=0;
S=[s0 s1];
t1=0;
T=[t1];
aobs=0;
%%
%Gm
bm1=0.5;
am1=-0.3;
am2=-0.2;
%%


    [output,ia,time]=VirtualMotor(u(1),0);
    y(1)=output;
    [output,ia,time]=VirtualMotor(u(2),1);
    y(2)=output;
    
while i<N
    
    rin=5;
    [output,ia,time]=VirtualMotor(u(i),i);
    y(i)=output;
    phi=[-y(i-1) -y(i-2) u(i-1) u(i-2)];
    q=p/(lambda+phi*p*phi');
    k=q*phi';
    p=(1/lambda)*(p-(p*(phi')*phi*p)/(1+phi*p*(phi')));
    theta=theta+k*(y(i)-phi*theta);
    yest(i)=phi*theta;
    error(i)=y(i)-yest(i);
    i=i+1;
    a1=theta(1);
    a2=theta(2);
    b1=theta(3);
    b2=theta(4);
%     num=[b1 b2];
%     den=[1 a1 a2];
%     Gz=tf(num,den,0.1);
     A=[0 0 b1 0 0;
        0 0 b2 0 -bm1;
        b1 0 0 1 -1;
        b2 b1 0 a1 -am1;
        0 b2 0 a2 -am2;];
     B=[bm1 0 am1 -a2+am2 0];
     x=pinv(A)*B';
  
    
     
    
    
end
%error=y-yest; 
% figure (1)
% plot (error)
%     figure (2)
%     plot (y,'b')
%      hold on
%      plot (yest,'r')
% % % %     figure (2)
% % % %     plot (y,'b')
% % % %     figure (3)
% % % %     plot (yest,'r')