N=1000;%samples
u=zeros(N,1);
y=zeros(N,1);
yest=zeros(N,1);
error=zeros(N,1);
npar=4;
theta=zeros(1,npar)';
alpha=100;
p=alpha*eye(npar);
q=eye(npar);
lambda=0.98;
i=3;


    [output,ia,time]=VirtualMotor(u(1),0);
    %y(1)=output(1);
    [output,ia,time]=VirtualMotor(u(2),1);
    %y(2)=output(2);
    
while i<N
    u(i)=5;
    [output,ia,time]=VirtualMotor(u(i),i);
    y(i)=output;
    phi=[-y(i-1) -y(i-2) u(i-1) u(i-2)];
    q=p/(lambda+phi*p*phi');
    k=q*phi';
    p=(1/lambda)*(p-(p*(phi')*phi*p)/(1+phi*p*(phi')));
    theta=theta+k*(y(i)-phi*theta);
    yest(i)=phi*theta;
    error(i)=y(i)-yest(i);
    i=i+1;
    
end
%error=y-yest;
figure (1)
plot (error)
    figure (2)
    plot (y,'b')
     hold on
     plot (yest,'r')
% % %     figure (2)
% % %     plot (y,'b')
% % %     figure (3)
% % %     plot (yest,'r')