wnm=3;%frequency in the s domain 

%% Initialization

% inputflag = 1: squarewave
% inputflag = 2: step
% inputflag = 3: 100 up 100 down
% inputflag = 4: simulation input
inputflag = 3
if inputflag==1
N=500;%samples

npar=4;
y=zeros(N,1);
% signal generation
x = -pi:4*pi/N:3*pi-4*pi/N;
u = square(x)';


elseif inputflag==2
    
    
N=50;
npar=4;
y=zeros(N,1);
u = ones(N);


elseif inputflag==3
    
    
N=700;
u=zeros(N,1);
npar=4;
y=zeros(N,1);
study_response=zeros(N,5);
study_voltage=zeros(N,5);
u(1:200) = 300;
u(201:400) = 0;
u(401:end) = 300;
 
 
else
    
    
%Reference Signal Generation
N=1000;
u=zeros(N,1);
npar=4;
y=zeros(N,1);

 u(1:200) = 300;
 u(201:400) = 900;
 u(401:600) = 1200;
 u(601:800) = 600;
 u(801:end) = 0;
end
for j=1:5
%% Plot arrays initialization
yest=zeros(N,1);
error=zeros(N,1);
ue_motor_input=ones(N,1);
thetaLong=zeros(N,npar);

%% parameter initialization
alpha = 100;
p = eye(npar)*alpha;
phi = zeros(npar,1);
theta = 0.5*ones(npar,1);
forgFactor = 1;
%forgetting factor
lambda=1;
i=3;
h=0.1;

%% desired behavior
wnm=j;%frequency in the s domain 
xim=1;%damping in the s domain 
%according to page 21 Am(q)=
am1=-2*exp(-xim*wnm*h)*cos(wnm*sqrt(1-xim^2)*h);
am2=exp(-2*xim*wnm*h);


%% observer
wn_obs=8 %frequency in the s domain -> should be faster then gm
xi_obs=1;%damping factor in the s domain 
%conversion s ->z
aobs1=-2*exp(-xi_obs*wn_obs*h)*cos(wn_obs*sqrt(1-xi_obs^2)*h); 
aobs2=exp(-2*xi_obs*wn_obs*h);
%% first 2 steps, since we have to access later 
VirtualMotor(0,0);
VirtualMotor(0,1);
ue=1;
%%%%%%%%%%%%%%%%%%% RUN %%%%%%%%%%%%%%%%%%%%%%%%%%%
while i<N
%% simulation of the mtor 
  [output,ia,time]=VirtualMotor(ue,1);     
%% Identify System 
  % input of the motor is the ue calculated in the end of the while loop
  %recursiv least square with forgetting factor
   y(i,1)=output;
   phi=[-y(i-1) -y(i-2) ue_motor_input(i-1) ue_motor_input(i-2)]; % input and output of the motor
   q=p/(lambda+phi*p*phi');
   k=q*phi';
   p=(1/lambda)*(p-(p*(phi')*phi*p)/(1+phi*p*(phi')));
   yest(i)=phi*theta;%the estimated output
   theta=theta+k*(y(i)-yest(i));%contains the current coefficients
   error(i)=y(i)-yest(i);%plotting

%% gain new coeficients
    a1=theta(1);
    a2=theta(2);
    b1=theta(3);
    b2=theta(4);
    
    thetaLong(i,1)=a1;
    thetaLong(i,2)=a2;
    thetaLong(i,3)=b1;
    thetaLong(i,4)=b2;
%% adapt the controler
  % according to page 21
    bm_quote=(1+am1+am2)/(b1+b2);
  % according to page 23 
    P0=am1+aobs1+1-a1;
    P1=a2+a1*P0-am1-am2-aobs2-aobs1*(1+am1)-1;
    P2=b1*(1-a1)+b2;
    P3=a2*(P0-1)-a1*P0-am1*aobs2-am2*aobs1;
    P4=b1*(a1-a2);
    P5=-a2*P0-am2*aobs2;
    P6=-P3*P2+P4*P1;
   % caclutating new rst parameter
    s1=-(b1*(P5*P2-P1*a2*b1)+b2*P6)/(b2*(P4*b1-P2*b2)-a2*b1^3);
    s2=(P6+(P4*b1-P2*b2)*s1)/(P2*b1);
    s0=-(P1+b1*s1)/P2;
    r1=P0-b1*s0;
    
 %% calculate the new input for the motor
   % according to page 24 -> very end
    u_after_filter=bm_quote*(u(i)+aobs1*u(i-1)+aobs2*u(i-2));%filter 
    motor_output_feedback=s0*y(i)+s1*y(i-1)+s2*y(i-2) ;
    motor_input_feedback=(r1-1)*ue_motor_input(i-1)-r1*ue_motor_input(i-2);
    ue=u_after_filter-motor_output_feedback-motor_input_feedback;
   %check if the limits are exceeded 
    if abs(ue)>20
        ue=sign(ue)*20;
    end
    ue_motor_input(i)=ue;
 % increase the counter
    i=i+1; 
    
end
study_response(:,j)=yest(:);
study_voltage(:,j)=ue_motor_input(:);
end
%% obatined G
num=[theta(4,1), theta(3,1)];
dnum= [1 ,theta(1,1), theta(2,1)];
G=tf(num,dnum,0.1)
% figure(6)
% step(G)

%% plot
 plottime=linspace(40,50,100);
% figure (1)
% % plot error
% plot (error)
% plot behaviour and desired input
figure (2)
title('Responses with different frequencies')
xlabel('w_rpm') % x-axis label
ylabel('time') % y-axis label
%plot (plottime,y,'b')
hold on
plot (plottime,study_response(400:499,1))
hold on 
plot (plottime,study_response(400:499,2))
hold on 
plot (plottime,study_response(400:499,3))
hold on 
plot (plottime,study_response(400:499,4))
hold on 
plot (plottime,study_response(400:499,5))
hold on 
plot (plottime,u(400:499),'k')
figure(3)
hold on
%plot(plottime,ue_motor_input,'y')
% % plot thetas
% figure(4)
% hold on
% plot(thetaLong(:,1))
% plot(thetaLong(:,2))
% plot(thetaLong(:,3))
% plot(thetaLong(:,4))
 