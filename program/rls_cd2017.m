function [th,P,e] = rls_cd2017(th,Phi,P,y,npar,forgFactor)
    I = eye(npar);
    K = (P*Phi)/((1*forgFactor)+Phi'*P*Phi);
    P = (1/forgFactor)*(I - K*Phi')*P;
    e = y - Phi'*th;
    th = th+K*e;
end