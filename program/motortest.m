x=linspace(0,10,100);
N=100;
x = -pi:4*pi/N:3*pi-4*pi/N;
u = square(x)';
u = ones(N);
simulation_time=length(x);
i=1;
result=zeros(3,length(x));
VirtualMotor(1,0);
while(i<simulation_time)
    
       [w_rpm,ia,t] = VirtualMotor(u(i),i);
       result(1,i+1)= w_rpm;   
       result(2,i+1)= ia;   
       result(3,i+1)= t; 
       i=i+1;
    
end
figure(1)
plot(result(1,:));
figure(2)
plot(result(2,:));
figure(3)
plot(u)
%plot(x,result(3,:));